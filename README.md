# Counter APP #

Counter app for use on conferences organized by Kavli Institute for Systems Neuroscience/Centre for Neural Computation, NTNU. The app is used for time management during talks and discussions. It has two modes of operation:

* A countdown timer on green background (should be used during the talk).
* A count-up timer on red background (should be used during discussion). Once countdown is finished, the count-up starts. 

User can select time in minutes for both modes via settings.

### How to use ###

The interaction with application is performed via keyboard shortcuts:

* F - toggle fullscreen mode.
* H - show help dialog.
* S - start or stop countdown.
* Ctrl+P - show preferences dialog.

Additionally there is a Start/Stop button, which is used to start and stop countdown timer.